import 'dart:typed_data';

import 'package:uor_keyring/transform/transform.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart';

extension AdvancedIter<T> on List<T> {
  List<V> mapWithIndex<V>(V Function(T item, int index) callback) {
    List<V> result = [];
    for (int i = 0; i < length; i++) {
      result.add(callback(this[i], i));
    }
    return result;
  }

  bool everyWithIndex(bool Function(T item, int index) callback) {
    bool result = true;
    for (int i = 0; i < length; i++) {
      if (!callback(this[i], i)) {
        result = false;
        break;
      }
    }
    return result;
  }
}

extension AdvancedString on String {
  String toBase64() {
    List<int> bytes = utf8.encode(this);
    return base64.encode(bytes);
  }

  String fromBase64() {
    List<int> bytes = base64.decode(this);
    return utf8.decode(bytes);
  }

  String encrypt(String password) {
    final List<int> keyBytes = utf8.encode(password);
    final Digest hashedPassword = sha256.convert(keyBytes);
    final Key key = Key(hashedPassword.bytes as Uint8List);
    final IV iv = IV.fromSecureRandom(16);

    final encrypter = Encrypter(AES(key, mode: AESMode.ctr, padding: null));
    final encrypted = encrypter.encrypt(this, iv: iv);

    // Combine IV and encrypted data
    return '${iv.base64}:${encrypted.base64}';
  }

  String decrypt(String password) {
    // Split IV and encrypted data
    final parts = split(':');
    if (parts.length != 2) {
      throw Exception('Invalid encrypted data format');
    }

    final List<int> keyBytes = utf8.encode(password);
    final Digest hashedPassword = sha256.convert(keyBytes);
    final key = Key(hashedPassword.bytes as Uint8List);
    final iv = IV.fromBase64(parts[0]);
    final encrypter = Encrypter(AES(key, mode: AESMode.ctr, padding: null));
    return encrypter.decrypt64(parts[1], iv: iv);
  }
}

typedef ProcessActionMethod = void Function(
  Transformable action,
);
