import 'package:hive/hive.dart';
import 'package:uor_keyring/widgets/decoder/data/alias_item.dart';

class AliasStorage {
  late final Box box;

  static Future<void> init() async {
    Hive.registerAdapter(AliasItemHiveAdapter());
    await Hive.openBox('aliases');
  }

  AliasStorage() {
    box = Hive.box('aliases');
  }

  bool add(AliasItem aliasItem) {
    String? found = box.toMap().keys.firstWhere(
          (alias) => alias == aliasItem.alias,
          orElse: () => null,
        );
    if (found != null) {
      return false;
    }
    box.put(aliasItem.alias, aliasItem);
    return true;
  }

  List<AliasItem> get() {
    List<AliasItem> keys =
        Map<String, AliasItem>.from(box.toMap()).values.toList();
    keys.sort((t1, t2) => t1.createdAt.compareTo(t2.createdAt));
    return keys;
  }
}
