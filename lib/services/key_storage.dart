import 'package:hive/hive.dart';
import 'package:uor_keyring/widgets/keystore/data/key_item.dart';

class KeyStorage {
  late final Box keyring;

  static Future<void> init() async {
    Hive.registerAdapter(KeyItemHiveAdapter());
    await Hive.openBox('keys');
  }

  KeyStorage() {
    keyring = Hive.box('keys');
  }

  bool add(KeyItem keyItem) {
    String? found = keyring.toMap().keys.firstWhere(
          (name) => name == keyItem.name,
          orElse: () => null,
        );
    if (found != null) {
      return false;
    }
    keyring.put(keyItem.name, keyItem);
    return true;
  }

  List<KeyItem> get() {
    List<KeyItem> keys =
        Map<String, KeyItem>.from(keyring.toMap()).values.toList();
    keys.sort((t1, t2) => t1.createdAt.compareTo(t2.createdAt));
    return keys;
  }
}
