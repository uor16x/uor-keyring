import 'package:cherry_toast/cherry_toast.dart';
import 'package:cherry_toast/resources/arrays.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uor_keyring/theme.dart';

class CliboardHelper {
  static void copyToClipboard(String text, BuildContext context) {
    Clipboard.setData(ClipboardData(text: text));
    CherryToast.success(
      animationType: AnimationType.fromTop,
      backgroundColor: Styles.getTheme().scaffoldBackgroundColor,
      displayCloseButton: false,
      animationDuration: const Duration(milliseconds: 500),
      toastDuration: const Duration(seconds: 2),
      title: const Text("Copied to clipboard"),
    ).show(context);
  }
}
