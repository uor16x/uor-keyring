import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:uor_keyring/main.dart';

const Color mainColor = Color.fromARGB(44, 98, 110, 124);
const Color mainOverlappingColor = Color.fromARGB(255, 26, 30, 34);
const Color secondaryColor = Color.fromARGB(225, 255, 255, 255);

class Styles {
  static final BoxDecoration boxDecoration = BoxDecoration(
    borderRadius: BorderRadius.circular(5),
    border: Border.all(
      color: Colors.white54,
    ),
  );

  static SizedBox emptySpace([double height = 5]) => SizedBox(height: height);

  static EdgeInsets padding([double all = 10]) => EdgeInsets.all(all);

  static EdgeInsets inputPadding(
    BuildContext context, {
    int topOffset = 0,
    int rightOffset = 0,
    int bottomOffset = 0,
    int leftOffset = 0,
  }) {
    EdgeInsets viewInsets = MediaQuery.of(context).viewInsets;
    return EdgeInsets.only(
      top: viewInsets.top + topOffset,
      right: viewInsets.right + rightOffset,
      bottom: viewInsets.bottom + bottomOffset,
      left: viewInsets.left + leftOffset,
    );
  }

  static ThemeData getTheme() {
    return ThemeData.dark().copyWith(
      floatingActionButtonTheme: const FloatingActionButtonThemeData(
        backgroundColor: mainColor,
        foregroundColor: secondaryColor,
      ),
      tabBarTheme: TabBarTheme(
        labelColor: secondaryColor,
        unselectedLabelColor: secondaryColor,
        indicatorColor: secondaryColor,
        overlayColor: WidgetStateProperty.all(mainColor),
      ),
      cardTheme: const CardTheme(
        color: mainColor,
      ),
      canvasColor: mainOverlappingColor,
      bottomSheetTheme: const BottomSheetThemeData(
        backgroundColor: mainOverlappingColor,
      ),
      inputDecorationTheme: const InputDecorationTheme(
        fillColor: mainColor,
        filled: true,
        focusColor: mainOverlappingColor,
        hoverColor: mainOverlappingColor,
        hintStyle: TextStyle(
          color: secondaryColor,
          decorationColor: secondaryColor,
          backgroundColor: secondaryColor,
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: secondaryColor,
          ),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(
            color: secondaryColor,
          ),
        ),
      ),
      indicatorColor: secondaryColor,
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          foregroundColor: WidgetStateProperty.all(secondaryColor),
          padding: WidgetStateProperty.all(
            const EdgeInsets.only(top: 12, bottom: 12),
          ),
          shape: WidgetStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(1.0),
              side: const BorderSide(color: secondaryColor, width: 1.0),
            ),
          ),
          backgroundColor: WidgetStateProperty.all(mainColor),
        ),
      ),
      appBarTheme: const AppBarTheme(
        color: mainColor,
      ),
      scaffoldBackgroundColor: mainColor,
    );
  }
}
