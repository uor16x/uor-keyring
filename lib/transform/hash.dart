import 'dart:convert';

import 'package:uor_keyring/shared/ordered_string_item.dart';
import 'package:uor_keyring/transform/transform.dart';
import 'package:crypto/crypto.dart';

TransformActionType? transformActionTypefromString(String key) {
  return TransformActionType.values
      .firstWhere((e) => e.toString().split('.').last == key);
}

class HashTransform implements Transformable {
  @override
  final OrderedStringItem input;
  @override
  late final List args;

  @override
  late final TransformActionType key;

  HashTransform(this.input, this.args) {
    if (args.length != 1) {
      throw ArgumentError('Hash expects 1 argument');
    }
    if (args[0].runtimeType != String) {
      throw ArgumentError(
          'Hash expects first argument(hashFunction) to be a string');
    }
    if (!['md5', 'sha1', 'sha256', 'sha512'].contains(args[0])) {
      throw ArgumentError(
          'Hash expects first argument(hashFunction) to be a valid hash function name');
    }
    TransformActionType? parsedType = transformActionTypefromString(args[0]);
    if (parsedType == null) {
      throw ArgumentError(
          'Failed to parse hash function name to TransformActionType');
    }
    key = parsedType;
  }

  @override
  String transform() {
    switch (key) {
      case TransformActionType.md5:
        return _md5(input.value);
      case TransformActionType.sha1:
        return _sha1(input.value);
      case TransformActionType.sha256:
        return _sha256(input.value);
      case TransformActionType.sha512:
        return _sha512(input.value);
      default:
        throw ArgumentError(
          'Hash expects second argument to be a valid hash function name',
        );
    }
  }

  String _md5(String text) {
    return md5.convert(utf8.encode(text)).toString();
  }

  String _sha1(String text) {
    return sha1.convert(utf8.encode(text)).toString();
  }

  String _sha256(String text) {
    return sha256.convert(utf8.encode(text)).toString();
  }

  String _sha512(String text) {
    return sha512.convert(utf8.encode(text)).toString();
  }
}
