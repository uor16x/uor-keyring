import 'package:uor_keyring/shared/ordered_string_item.dart';
import 'package:uor_keyring/transform/attach.dart';
import 'package:uor_keyring/transform/concat.dart';
import 'package:uor_keyring/transform/none.dart';
import 'package:uor_keyring/transform/substr.dart';
import 'package:uor_keyring/transform/hash.dart';

enum TransformActionType {
  none,
  substr,
  concat,
  attach,
  md5,
  sha1,
  sha256,
  sha512,
}

extension ParseToString on TransformActionType {
  String asString() {
    return toString().split('.').last;
  }
}

abstract class Transformable {
  final TransformActionType key = TransformActionType.none;
  final OrderedStringItem input = OrderedStringItem(0, '');
  final List args = [];
  static Map<TransformActionType,
      Transformable Function(OrderedStringItem input, List args)> keyTypes = {
    TransformActionType.none: (OrderedStringItem input, List args) =>
        NoneTransform(input.value),
    TransformActionType.attach: AttachTransform.new,
    TransformActionType.concat: ConcatTransform.new,
    TransformActionType.substr: SubstrTransform.new,
    TransformActionType.md5: (OrderedStringItem input, List args) =>
        HashTransform(
          input,
          ['md5'],
        ),
    TransformActionType.sha1: (OrderedStringItem input, List args) =>
        HashTransform(
          input,
          ['sha1'],
        ),
    TransformActionType.sha256: (OrderedStringItem input, List args) =>
        HashTransform(
          input,
          ['sha256'],
        ),
    TransformActionType.sha512: (OrderedStringItem input, List args) =>
        HashTransform(
          input,
          ['sha512'],
        ),
  };
  String transform();
}
