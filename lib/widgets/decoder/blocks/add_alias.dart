import 'package:flutter/material.dart';
import 'package:uor_keyring/theme.dart';

class AddAlias extends StatefulWidget {
  final void Function(String alias, String keyword) onAdd;
  const AddAlias({super.key, required this.onAdd});

  @override
  State<AddAlias> createState() => _AddAliasState();
}

class _AddAliasState extends State<AddAlias> {
  String alias = '';
  String keyword = '';

  void setAlias(String? value) {
    setState(() {
      alias = value ?? '';
    });
  }

  void setKeyword(String? value) {
    setState(() {
      keyword = value ?? '';
    });
  }

  void Function()? getAddCallback() {
    return alias.isNotEmpty && keyword.isNotEmpty
        ? () => {
              widget.onAdd(alias, keyword),
              Navigator.pop(context),
            }
        : null;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: SingleChildScrollView(
        child: Container(
          padding: Styles.padding(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Text(
                "Add alias",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Styles.emptySpace(10),
              TextField(
                decoration: const InputDecoration(
                  labelText: 'Alias',
                ),
                onChanged: setAlias,
              ),
              Styles.emptySpace(10),
              TextField(
                decoration: const InputDecoration(
                  labelText: 'Keyword',
                ),
                onChanged: setKeyword,
              ),
              Styles.emptySpace(10),
              ElevatedButton(
                onPressed: getAddCallback(),
                onLongPress: null,
                child: const Text(
                  'Add',
                  style: TextStyle(fontSize: 24),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
