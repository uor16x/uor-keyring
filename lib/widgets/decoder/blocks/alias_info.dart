import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:uor_keyring/extensions.dart';
import 'package:uor_keyring/shared/generator.dart';
import 'package:uor_keyring/shared/log_items_collection.dart';
import 'package:uor_keyring/theme.dart';
import 'package:uor_keyring/widgets/decoder/data/alias_item.dart';
import 'package:uor_keyring/widgets/keystore/data/key_item.dart';
import 'package:uor_keyring/widgets/shared/pattern_key.dart';
import 'package:uor_keyring/shared/clipboard.dart';

class _DecodeResult extends StatelessWidget {
  final String decoreErr;
  final String decodeResult;

  const _DecodeResult({
    required this.decoreErr,
    required this.decodeResult,
  });

  @override
  Widget build(BuildContext context) {
    List<Widget> result = [];
    if (decoreErr.isNotEmpty) {
      result.add(
        Container(
          padding: Styles.padding(5),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  decoreErr,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    fontSize: 18,
                    color: Colors.red,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
    if (decodeResult.isNotEmpty) {
      result.add(
        Container(
          padding: Styles.padding(5),
          child: Row(
            children: [
              Expanded(
                flex: 10,
                child: Text(
                  decodeResult,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    fontSize: 18,
                    color: Colors.green,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: IconButton(
                  onPressed: () {
                    CliboardHelper.copyToClipboard(decodeResult, context);
                  },
                  icon: const Icon(
                    Icons.copy,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
    return Container(
      padding: const EdgeInsets.only(
        left: 15,
        right: 15,
        top: 0,
        bottom: 15,
      ),
      child: DottedBorder(
        color: Colors.white,
        strokeWidth: 0.5,
        radius: const Radius.circular(12),
        borderType: BorderType.RRect,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: result,
        ),
      ),
    );
  }
}

class AliasInfo extends StatefulWidget {
  final AliasItem aliasItem;
  final List<DropdownMenuItem<KeyItem>> keyItems;
  final Function() addKeyLink;
  void Function(AliasItem item) onDelete;

  AliasInfo(
    this.aliasItem, {
    required this.onDelete,
    required this.keyItems,
    required this.addKeyLink,
    super.key,
  });

  @override
  State<AliasInfo> createState() => _AliasInfoState();
}

class _AliasInfoState extends State<AliasInfo> {
  KeyItem? selectedKey;
  String decoreErr = '';
  String decodeResult = '';

  void select(KeyItem? value) {
    setState(() {
      selectedKey = value;
    });
  }

  void decode() {
    setState(() {
      decoreErr = '';
      decodeResult = '';
    });
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Set pattern key to encrypt your key'),
          content: PatternKey(
            actionLabel: 'Save',
            onAction: (List<int> pattern) {
              Navigator.of(context).pop();
              String sequence = selectedKey!.encodedKey;
              if (sequence.contains('ENC_')) {
                sequence =
                    sequence.replaceAll('ENC_', '').decrypt(pattern.join('-'));
              }
              if (!sequence.contains('KEY_')) {
                return setState(() {
                  decoreErr = 'Failed to decode key';
                });
              }
              LogItemsCollection appliedSequence = Generator.applyKey(
                widget.aliasItem.keyword,
                sequence,
              );
              setState(() {
                decodeResult = appliedSequence.getResult();
              });
            },
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget keySelect = widget.keyItems.isEmpty
        ? Row(
            children: [
              const Expanded(
                flex: 8,
                child: Text(
                  'No keys available to decode',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15),
                ),
              ),
              Expanded(
                flex: 2,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    side: const BorderSide(
                      width: 1.0,
                      color: Colors.white,
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                  onPressed: widget.addKeyLink,
                  onLongPress: null,
                  child: const Text(
                    'Add',
                    style: TextStyle(fontSize: 24),
                  ),
                ),
              ),
            ],
          )
        : Container(
            decoration: const ShapeDecoration(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
            ),
            child: DropdownButton<KeyItem>(
              isExpanded: true,
              value: selectedKey,
              icon: const Icon(Icons.fact_check),
              elevation: 16,
              style: const TextStyle(color: Colors.white),
              underline: Container(
                height: 2,
                color: Colors.white30,
              ),
              onChanged: select,
              items: widget.keyItems,
            ),
          );

    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.transparent,
      body: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Padding(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom + 20,
          ),
          child: Container(
            padding: Styles.padding(),
            margin: const EdgeInsets.only(bottom: 10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding: const EdgeInsets.only(left: 15, right: 15, bottom: 5),
                  child: Text(
                    widget.aliasItem.alias,
                    textAlign: TextAlign.left,
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  child: Text(
                    widget.aliasItem.keyword,
                    textAlign: TextAlign.left,
                    style: const TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 15,
                        child: keySelect,
                      ),
                      const Expanded(
                        flex: 1,
                        child: SizedBox(
                          width: 0,
                        ),
                      ),
                      Expanded(
                        flex: 6,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            side: const BorderSide(
                              width: 1.0,
                              color: Colors.white,
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12),
                            ),
                          ),
                          onPressed: selectedKey == null ? null : decode,
                          onLongPress: null,
                          child: const Text(
                            'Decode',
                            style: TextStyle(fontSize: 24),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Offstage(
                  offstage: decoreErr.isEmpty && decodeResult.isEmpty,
                  child: _DecodeResult(
                    decoreErr: decoreErr,
                    decodeResult: decodeResult,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      side: const BorderSide(
                        width: 1.0,
                        color: Colors.white,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                    ),
                    onPressed: () => widget.onDelete(widget.aliasItem),
                    onLongPress: null,
                    child: const Text(
                      'Delete',
                      style: TextStyle(fontSize: 24),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
