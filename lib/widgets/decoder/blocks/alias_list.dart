import 'package:flutter/material.dart';
import 'package:uor_keyring/extensions.dart';
import 'package:uor_keyring/theme.dart';
import 'package:uor_keyring/widgets/decoder/blocks/alias_info.dart';
import 'package:uor_keyring/widgets/decoder/data/alias_item.dart';
import 'package:uor_keyring/widgets/keystore/data/key_item.dart';

class _AliasListItem extends StatelessWidget {
  final AliasItem aliasItem;
  final int itemIndex;
  final List<DropdownMenuItem<KeyItem>> keyItems;
  final Function() addKeyLink;
  void Function(AliasItem item) onDelete;

  _AliasListItem(
    this.aliasItem,
    this.itemIndex,
    this.keyItems, {
    required this.onDelete,
    required this.addKeyLink,
  });

  static const TextStyle textStyle = TextStyle(
    fontSize: 20,
  );

  showInfo(BuildContext context) {
    _AliasListItem listItem = context.widget as _AliasListItem;
    showModalBottomSheet(
      context: context,
      builder: (context) => AliasInfo(
        listItem.aliasItem,
        onDelete: onDelete,
        keyItems: keyItems,
        addKeyLink: addKeyLink,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => showInfo(context),
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: Styles.boxDecoration,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              flex: 1,
              child: Center(
                child: Text(
                  '#${itemIndex + 1}',
                  style: textStyle,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            Expanded(
              flex: 8,
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  aliasItem.alias,
                  style: textStyle,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Aliaslist extends StatelessWidget {
  final List<AliasItem> aliases;
  final List<KeyItem> keys;
  final Function() addKeyLink;
  final void Function(AliasItem item) onDelete;

  const Aliaslist({
    required this.aliases,
    required this.keys,
    required this.onDelete,
    required this.addKeyLink,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    List<DropdownMenuItem<KeyItem>> keyItems = keys
        .mapWithIndex<DropdownMenuItem<KeyItem>>(
          (item, index) => DropdownMenuItem<KeyItem>(
            value: item,
            child: Text(
              '#${index + 1}: ${item.name}',
              overflow: TextOverflow.ellipsis,
            ),
          ),
        )
        .toList();
    List<_AliasListItem> items = aliases.mapWithIndex(
      (item, index) => _AliasListItem(
        item,
        index,
        keyItems,
        addKeyLink: addKeyLink,
        onDelete: onDelete,
      ),
    );
    if (items.isNotEmpty) {
      return ListView.builder(
          padding: Styles.padding(),
          itemCount: items.length,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemBuilder: (context, index) => Card(child: items[index]));
    } else {
      return const SizedBox.shrink();
    }
  }
}
