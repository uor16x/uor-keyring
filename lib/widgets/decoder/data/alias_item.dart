import 'package:hive/hive.dart';

@HiveType(typeId: 1)
class AliasItem extends HiveObject {
  @HiveField(0)
  final String alias;

  @HiveField(1)
  final String keyword;

  @HiveField(2)
  final DateTime createdAt;

  AliasItem(this.alias, this.keyword, this.createdAt);
}

class AliasItemHiveAdapter extends TypeAdapter<AliasItem> {
  @override
  final typeId = 1;

  @override
  AliasItem read(BinaryReader reader) {
    return AliasItem(
      reader.read(),
      reader.read(),
      DateTime.now(),
    );
  }

  @override
  void write(BinaryWriter writer, AliasItem obj) {
    writer.write(obj.alias);
    writer.write(obj.keyword);
    writer.write(obj.createdAt.microsecondsSinceEpoch);
  }
}
