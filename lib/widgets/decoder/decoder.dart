import 'package:flutter/material.dart';
import 'package:uor_keyring/services/alias_storage.dart';
import 'package:uor_keyring/services/key_storage.dart';
import 'package:uor_keyring/shared/confirm_dialog.dart';
import 'package:uor_keyring/theme.dart';
import 'package:uor_keyring/widgets/decoder/blocks/add_alias.dart';
import 'package:uor_keyring/widgets/decoder/blocks/alias_list.dart';
import 'package:uor_keyring/widgets/decoder/data/alias_item.dart';
import 'package:uor_keyring/widgets/keystore/data/key_item.dart';
import 'package:uor_keyring/widgets/shared/tab_header.dart';

class Decoder extends StatefulWidget {
  final Function(int index) switchTab;
  const Decoder(this.switchTab, {super.key});

  @override
  State<Decoder> createState() => _DecoderState();
}

class _DecoderState extends State<Decoder> {
  AliasStorage aliasStorage = AliasStorage();
  KeyStorage keyStorage = KeyStorage();

  void onAddAlias(String alias, String keyword) {
    aliasStorage.add(AliasItem(
      alias,
      keyword,
      DateTime.now(),
    ));
    setState(() {});
  }

  void showDeleteDialog(BuildContext buildContext, AliasItem aliasItem) {
    showDialog(
      context: context,
      builder: (context) => ConfirmDialog(
        title: "Delete",
        message: "Are you sure you want to delete the alias?",
        onConfirm: () {
          aliasItem.delete();
          setState(() {});
          Navigator.of(context).pop();
        },
      ),
    );
  }

  void showAddActionModal(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      builder: (context) => AddAlias(onAdd: onAddAlias),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> buttons = [];
    buttons.add(
      FloatingActionButton.extended(
        onPressed: () => showAddActionModal(context),
        label: const Text('Add'),
        icon: const Icon(Icons.post_add),
      ),
    );

    List<AliasItem> aliases = aliasStorage.get();
    List<KeyItem> keys = keyStorage.get();

    return Scaffold(
      resizeToAvoidBottomInset: false,
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: buttons,
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            const TabHeader("Manage aliases"),
            Styles.emptySpace(),
            Expanded(
              child: Aliaslist(
                aliases: aliases,
                keys: keys,
                addKeyLink: () =>
                    {Navigator.of(context).pop(), widget.switchTab(1)},
                onDelete: (item) => showDeleteDialog(context, item),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
