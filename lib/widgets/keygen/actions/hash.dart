import 'package:flutter/material.dart';
import 'package:uor_keyring/extensions.dart';
import 'package:uor_keyring/theme.dart';
import 'package:uor_keyring/transform/hash.dart';
import 'package:uor_keyring/shared/action_result.dart';
import 'package:uor_keyring/transform/transform.dart';
import 'package:uor_keyring/widgets/keygen/actions/select_input.dart';
import 'package:uor_keyring/shared/ordered_string_item.dart';

class Hash extends StatefulWidget {
  final List<ActionLogItem> inputs;
  final String hashFunction;
  final ProcessActionMethod onTransform;

  const Hash({
    super.key,
    required this.inputs,
    required this.hashFunction,
    required this.onTransform,
  });

  @override
  State<Hash> createState() => _HashState();
}

class _HashTransformButton extends StatelessWidget {
  Function()? onClick;

  _HashTransformButton({required this.onClick});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onClick,
      onLongPress: null,
      child: const Text(
        'Hash',
        style: TextStyle(fontSize: 24),
      ),
    );
  }
}

class _HashState extends State<Hash> {
  OrderedStringItem? selectedValue;

  void setValue(OrderedStringItem? value) {
    setState(() {
      selectedValue = value;
    });
  }

  void transform() {
    try {
      Transformable action = HashTransform(
        selectedValue!,
        [widget.hashFunction],
      );
      widget.onTransform(action);
    } catch (err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> valueSelectedWidgets = <Widget>[
      _HashTransformButton(onClick: transform),
    ];

    return Container(
      decoration: Styles.boxDecoration,
      padding: Styles.padding(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SelectInput(inputs: widget.inputs, onSelect: setValue),
          Styles.emptySpace(),
          if (selectedValue != null) ...valueSelectedWidgets
        ],
      ),
    );
  }
}
