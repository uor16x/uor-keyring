import 'package:flutter/material.dart';
import 'package:uor_keyring/extensions.dart';
import 'package:uor_keyring/theme.dart';
import 'package:uor_keyring/widgets/shared/pattern_key.dart';

class Encrypt extends StatefulWidget {
  final String sequenceKey;
  final void Function(String name, String encryptedKey) onSave;

  const Encrypt({
    super.key,
    required this.sequenceKey,
    required this.onSave,
  });

  @override
  State<Encrypt> createState() => _EncryptState();
}

class _EncryptState extends State<Encrypt> {
  String name = '';
  String password = '';
  List<int> pattern = [];

  void setName(String value) {
    setState(() {
      name = value;
    });
  }

  void setPassword(String value) {
    setState(() {
      password = value;
    });
  }

  void setPattern(List<int> value) {
    setState(() {
      pattern = value;
    });
  }

  void process() {
    String encryptedKey = pattern.isEmpty
        ? widget.sequenceKey.toBase64()
        : 'ENC_${widget.sequenceKey.encrypt(pattern.join('-'))}';
    widget.onSave(name, encryptedKey);
  }

  String getPatternKeyText() {
    return pattern.isEmpty ? "Set pattern key" : "Change pattern key";
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> items = [
      const Text(
        "Encrypt and save your key",
        style: TextStyle(fontSize: 20),
      ),
      TextField(
        cursorColor: Colors.white,
        keyboardType: TextInputType.text,
        decoration: const InputDecoration(
          hintText: 'name (required)',
        ),
        onChanged: setName,
        textInputAction: TextInputAction.next,
      ),
      const SizedBox(
        height: 15,
      ),
      ElevatedButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: const Text('Set pattern key to encrypt your key'),
                content: PatternKey(
                  actionLabel: 'Save',
                  confirmRequired: true,
                  onAction: (List<int> pattern) {
                    setPattern(pattern);
                    Navigator.of(context).pop();
                  },
                ),
              );
            },
          );
        },
        onLongPress: null,
        child: Text(
          getPatternKeyText(),
          style: const TextStyle(fontSize: 24),
        ),
      ),
      const SizedBox(
        height: 15,
      ),
      ElevatedButton(
        onPressed: name.isEmpty ? null : process,
        onLongPress: null,
        child: const Text(
          "Save",
          style: TextStyle(fontSize: 24),
        ),
      ),
      // TextField(
      //   cursorColor: Colors.white,
      //   keyboardType: TextInputType.text,
      //   decoration: const InputDecoration(
      //     hintText: 'password (optional)',
      //   ),
      //   onChanged: setPassword,
      //   textInputAction: TextInputAction.next,
      // ),
      // const SizedBox(
      //   height: 15,
      // ),
    ];

    return Container(
      padding: Styles.padding(20),
      child: ListView(
        shrinkWrap: true,
        padding: MediaQuery.of(context).viewInsets,
        children: items,
      ),
    );
  }
}
