import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:uor_keyring/services/key_storage.dart';
import 'package:uor_keyring/shared/generator.dart';
import 'package:uor_keyring/shared/log_items_collection.dart';
import 'package:uor_keyring/shared/confirm_dialog.dart';
import 'package:uor_keyring/transform/none.dart';
import 'package:uor_keyring/transform/transform.dart';
import 'package:uor_keyring/widgets/keygen/blocks/add_action.dart';
import 'package:uor_keyring/widgets/keygen/blocks/encrypt.dart';
import 'package:uor_keyring/widgets/keygen/blocks/log_block.dart';
import 'package:uor_keyring/theme.dart';
import 'package:uor_keyring/widgets/keystore/data/key_item.dart';
import 'package:uor_keyring/widgets/shared/tab_header.dart';

class Keygen extends StatefulWidget {
  final Function(int index) switchTab;
  const Keygen(this.switchTab, {super.key});

  @override
  State<Keygen> createState() => _KeyGenState();
}

class _KeyGenState extends State<Keygen> {
  late LogItemsCollection log;
  KeyStorage keyStorage = KeyStorage();

  void reset({initial = false}) {
    const String initialResultText = 'my-email-1';
    log = LogItemsCollection();
    Transformable action = NoneTransform(initialResultText);
    log.add(action);
    if (!initial) {
      setState(() {
        log = log;
      });
    }
  }

  void showResetDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => ConfirmDialog(
        title: "Reset",
        message: "Are you sure you want to reset?",
        onConfirm: () => reset(),
      ),
    );
  }

  _KeyGenState() {
    reset(initial: true);
  }

  void addAction(BuildContext context, Transformable action) {
    Navigator.of(context).pop(); // close modal
    setState(() {
      log.add(action);
    });
  }

  void showSaveModal(BuildContext context, {int itemIndex = -1}) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) => Encrypt(
        sequenceKey: Generator.getKey(log, maxIndex: itemIndex),
        onSave: saveResultKey,
      ),
    );
  }

  void showAddActionModal(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) => AddAction(
        inputs: log.items,
        apply: (Transformable action) => addAction(context, action),
        onCancel: () => {},
      ),
    );
  }

  void saveResultKey(String name, String encryptedKey) {
    bool added = keyStorage.add(KeyItem(name, encryptedKey, DateTime.now()));
    if (added) {
      Navigator.of(context).pop();
      widget.switchTab(1);
    } else {
      showSimpleNotification(
        const Text('Key with this name already exists!'),
        background: const Color.fromARGB(255, 197, 55, 55),
      );
    }
    // Clipboard.setData(ClipboardData(text: encryptedKey));
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> buttons = [];
    if (log.items.length > 1) {
      buttons.add(
        FloatingActionButton.extended(
          onPressed: () => showResetDialog(context),
          label: const Text('Reset'),
          icon: const Icon(Icons.restart_alt),
        ),
      );
      buttons.add(
        FloatingActionButton.extended(
          onPressed: () => showSaveModal(context),
          label: const Text('Save'),
          icon: const Icon(Icons.save),
        ),
      );
    }

    buttons.add(
      FloatingActionButton.extended(
        onPressed: () => showAddActionModal(context),
        label: const Text('Add'),
        icon: const Icon(Icons.post_add),
      ),
    );

    List<Widget> buttonsWithPadding = buttons
        .map(
          (btn) => Padding(
            padding: const EdgeInsets.only(top: 15),
            child: btn,
          ),
        )
        .toList();

    return Scaffold(
      resizeToAvoidBottomInset: false,
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: buttonsWithPadding,
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            const TabHeader("Generate new key"),
            Styles.emptySpace(),
            Expanded(
              child: LogBlock(
                log.items,
                (itemIndex) => showSaveModal(context, itemIndex: itemIndex),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
