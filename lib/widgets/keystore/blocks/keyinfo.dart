import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uor_keyring/theme.dart';
import 'package:uor_keyring/widgets/keystore/data/key_item.dart';

class KeyInfo extends StatelessWidget {
  final KeyItem keyItem;
  void Function(KeyItem item) onDelete;

  KeyInfo(
    this.keyItem, {
    required this.onDelete,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: Styles.padding(),
      margin: const EdgeInsets.only(bottom: 10),
      child: ListView(
        shrinkWrap: true,
        children: [
          ListTile(
            title: Text(keyItem.name),
          ),
          Container(
            padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: const BorderSide(
                  width: 1.0,
                  color: Colors.white,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
              onPressed: () {
                Clipboard.setData(ClipboardData(text: keyItem.encodedKey));
                Navigator.of(context).pop();
              },
              onLongPress: null,
              child: const Text(
                'Copy',
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: const BorderSide(
                  width: 1.0,
                  color: Colors.white,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
              onPressed: () => onDelete(keyItem),
              onLongPress: null,
              child: const Text(
                'Delete',
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
