import 'package:flutter/material.dart';
import 'package:uor_keyring/extensions.dart';
import 'package:uor_keyring/theme.dart';
import 'package:uor_keyring/widgets/keystore/blocks/keyinfo.dart';
import 'package:uor_keyring/widgets/keystore/data/key_item.dart';

class _KeyListItem extends StatelessWidget {
  final KeyItem keyItem;
  final int itemIndex;
  void Function(KeyItem item) onDelete;

  _KeyListItem(
    this.keyItem,
    this.itemIndex, {
    required this.onDelete,
  });

  static const TextStyle textStyle = TextStyle(
    fontSize: 20,
  );

  showInfo(BuildContext context) {
    _KeyListItem listItem = context.widget as _KeyListItem;
    showModalBottomSheet(
      context: context,
      builder: (context) => KeyInfo(listItem.keyItem, onDelete: onDelete),
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => showInfo(context),
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: Styles.boxDecoration,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              flex: 2,
              child: Center(
                child: Text(
                  '#${itemIndex + 1}',
                  style: textStyle,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            Expanded(
              flex: 8,
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  keyItem.name,
                  style: textStyle,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Keylist extends StatelessWidget {
  final List<KeyItem> keys;
  final void Function(KeyItem item) onDelete;

  const Keylist({
    required this.keys,
    required this.onDelete,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    List<_KeyListItem> items = keys.mapWithIndex(
      (item, index) => _KeyListItem(
        item,
        index,
        onDelete: onDelete,
      ),
    );
    if (items.isNotEmpty) {
      return ListView.builder(
          padding: Styles.padding(),
          itemCount: items.length,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemBuilder: (context, index) => Card(child: items[index]));
    } else {
      return const SizedBox.shrink();
    }
  }
}
