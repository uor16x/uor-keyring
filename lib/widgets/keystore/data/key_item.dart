import 'package:hive/hive.dart';

@HiveType(typeId: 0)
class KeyItem extends HiveObject {
  @HiveField(0)
  final String name;

  @HiveField(1)
  final String encodedKey;

  @HiveField(2)
  final DateTime createdAt;

  KeyItem(this.name, this.encodedKey, this.createdAt);
}

class KeyItemHiveAdapter extends TypeAdapter<KeyItem> {
  @override
  final typeId = 0;

  @override
  KeyItem read(BinaryReader reader) {
    return KeyItem(
      reader.read(),
      reader.read(),
      DateTime.now(),
    );
  }

  @override
  void write(BinaryWriter writer, KeyItem obj) {
    writer.write(obj.name);
    writer.write(obj.encodedKey);
    writer.write(obj.createdAt.microsecondsSinceEpoch);
  }
}
