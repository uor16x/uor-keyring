import 'dart:math';

import 'package:flutter/material.dart';
import 'package:uor_keyring/services/key_storage.dart';
import 'package:uor_keyring/shared/confirm_dialog.dart';
import 'package:uor_keyring/theme.dart';
import 'package:uor_keyring/widgets/keystore/blocks/keylist.dart';
import 'package:uor_keyring/widgets/keystore/data/key_item.dart';
import 'package:uor_keyring/widgets/shared/tab_header.dart';

class Keystore extends StatefulWidget {
  final Function(int index) switchTab;
  const Keystore(this.switchTab, {super.key});

  @override
  State<Keystore> createState() => _KeystoreState();
}

class _KeystoreState extends State<Keystore> {
  KeyStorage keyStorage = KeyStorage();

  void showDeleteDialog(BuildContext buildContext, KeyItem keyItem) {
    showDialog(
      context: context,
      builder: (context) => ConfirmDialog(
        title: "Delete",
        message: "Are you sure you want to delete the key?",
        onConfirm: () {
          keyItem.delete();
          setState(() {});
          Navigator.of(context).pop();
        },
      ),
    );
  }

  String generateRandomString(int lengthOfString) {
    final random = Random();
    const allChars =
        'AaBbCcDdlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1EeFfGgHhIiJjKkL234567890';
    // below statement will generate a random string of length using the characters
    // and length provided to it
    final randomString = List.generate(lengthOfString,
        (index) => allChars[random.nextInt(allChars.length)]).join();
    return randomString; // return the generated string
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> buttons = [];
    buttons.add(
      FloatingActionButton.extended(
        onPressed: () => {
          keyStorage.add(KeyItem(
            generateRandomString(10),
            generateRandomString(10),
            DateTime.now(),
          )),
          setState(() {}),
        },
        label: const Text('Add'),
        icon: const Icon(Icons.create),
      ),
    );
    buttons.add(
      FloatingActionButton.extended(
        onPressed: () => widget.switchTab(2),
        label: const Text('Generate'),
        icon: const Icon(Icons.password),
      ),
    );

    List<Widget> buttonsWithPadding = buttons
        .map(
          (btn) => Padding(
            padding: const EdgeInsets.only(top: 15),
            child: btn,
          ),
        )
        .toList();

    List<KeyItem> keys = keyStorage.get();

    return Scaffold(
      resizeToAvoidBottomInset: false,
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: buttonsWithPadding,
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            const TabHeader("Manage stored keys"),
            Styles.emptySpace(),
            Expanded(
              child: Keylist(
                keys: keys,
                onDelete: (item) => showDeleteDialog(context, item),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
