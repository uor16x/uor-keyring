import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pattern_lock/pattern_lock.dart';
import 'package:uor_keyring/widgets/shared/tab_header.dart';

class PatternKey extends StatefulWidget {
  final String actionLabel;
  final bool confirmRequired;
  final void Function(List<int>) onAction;

  const PatternKey({
    required this.actionLabel,
    required this.onAction,
    this.confirmRequired = false,
    super.key,
  });

  @override
  State<PatternKey> createState() => _PatternKeyState();
}

class _PatternKeyState extends State<PatternKey> {
  bool confirmMode = false;
  List<int> pattern = [];

  String getHeader() {
    return confirmMode ? "Confirm pattern" : "Enter pattern";
  }

  void onInputComplete(List<int> input) {
    if (!widget.confirmRequired) {
      widget.onAction(input);
    } else if (confirmMode) {
      if (listEquals(pattern, input)) {
        widget.onAction(input);
      } else {
        setState(() {
          confirmMode = false;
        });
      }
    } else {
      setState(() {
        pattern = input;
        confirmMode = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      width: size.width * 0.5,
      height: size.height * 0.5,
      child: Column(
        children: [
          TabHeader(getHeader()),
          Expanded(
            child: PatternLock(
              selectedColor: Colors.grey,
              notSelectedColor: Colors.white,
              pointRadius: 5,
              showInput: true,
              dimension: 4,
              relativePadding: 0.7,
              selectThreshold: 25,
              fillPoints: true,
              onInputComplete: onInputComplete,
            ),
          ),
        ],
      ),
    );
  }
}
